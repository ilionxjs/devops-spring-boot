package com.ilionx.itph;

import com.ilionx.itph.persistance.opleiding.Beoordeling;
import com.ilionx.itph.persistance.opleiding.OpleidingsInstituut;
import com.ilionx.itph.persistance.opleiding.Studie;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class StudieTest {

    @Test
    public void getId() {
        System.out.println("getId");
        Studie instance = new Studie();
        instance.setId(0L);
        long expResult = 0L;
        long result = instance.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void setId() {
        System.out.println("setId");
        long id = 0L;
        OpleidingsInstituut instance = new OpleidingsInstituut();
        instance.setId(id);
    }

    @Test
    public void getBeoordelingen() {
        System.out.println("getBeoordelingen");
        Studie instance = new Studie() ;
        List<Beoordeling> expResult = null;
        List<Beoordeling> result = instance.getBeoordelingen();
        assertEquals ( expResult, result);
    }

    @Test
    public void setBeoordelingen() {
       System.out.println("setBeoordelingen");
       List<Beoordeling> beoordelings = null;
       Studie instance = new Studie();
       instance.setBeoordelingen(beoordelings);
    }


    @Test
    public void setOpleidingsinstituut() {
        System.out.println("setOpleidingsinstituut");
        List<OpleidingsInstituut> opleidingsInstituuts = null;
        Studie instance = new Studie();
        instance.setOpleidingsinstituut(opleidingsInstituuts);
    }

    @Test
    public void getOpleidingsinstituut() {
        System.out.println("getOpleidingsinstituut");
        Studie instance = new Studie();
        List<OpleidingsInstituut> expRes = null;
        List<OpleidingsInstituut> result = instance.getOpleidingsinstituut();
        assertEquals(expRes, result);
    }


    @Test
    public void setDuur() {
        System.out.println("setDuur");
        String duur = "";
        OpleidingsInstituut instance = new OpleidingsInstituut();
        instance.setNaam(duur);
    }

    @Test
    public void getDuur() {
        System.out.println("getDuur");
        Studie instance = new Studie();
        instance.setDuur("");
        String resExp = "";
        String result = instance.getDuur();
        assertEquals(resExp, result);
    }

    @Test
    public void setKosten() {
        System.out.println("setKosten");
        double kosten = 0 ;
        Studie instance = new Studie();
        instance.setKosten(kosten);
    }

    @Test
    public void getKosten() {
        System.out.println("getKosten");
        Studie instance = new Studie();
        instance.setKosten(0);
        double resExp = 0;
        double result = instance.getKosten();
        assertEquals(resExp, result, 0);
    }


    @Test
    public void getNaam() {
        System.out.println("getNaam");
        Studie instance = new Studie();
        instance.setNaam("");
        String resExp = "";
        String result = instance.getNaam();
        assertEquals(resExp, result);
    }

    @Test
    public void setNaam() {
        System.out.println("setNaam");
        String naam = "";
        OpleidingsInstituut instance = new OpleidingsInstituut();
        instance.setNaam(naam);
    }
}
