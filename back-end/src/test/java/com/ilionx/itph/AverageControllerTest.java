/*package com.ilionx.itph;

import com.ilionx.itph.controller.AverageController;
import com.ilionx.itph.persistance.opleiding.Beoordeling;
import com.ilionx.itph.persistance.opleiding.Studie;
import com.ilionx.itph.repository.BeoordelingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AverageControllerTest {

    @InjectMocks
    private AverageController ac;

    @Mock
    private BeoordelingRepository beoordelingRepository;

    @Before
    public void voorbereiding() {
    }

    @Test
    public void averagemeten() {

        ac = new AverageController(beoordelingRepository);
        String exres = "2";
        Studie studie = new Studie();
        studie.setNaam("testbaas");
        Beoordeling beoordeling = new Beoordeling();
        beoordeling.setStudie(studie);
        beoordeling.setRating(10);
        List<Beoordeling> L = new ArrayList<Beoordeling>();
        L.add(beoordeling);


        Mockito.when(beoordelingRepository.findAllByStudie_Id(2l)).thenReturn(L);


        ResponseEntity re = ac.average(7);
        assertEquals(exres, re.getBody().toString());
    }
}

*/