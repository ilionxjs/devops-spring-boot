update beoordeling b set student_id = s2.id
from student s1, student s2
where s1.gebruikersnaam = s2.gebruikersnaam
  and s1.id = b.student_id
  and s1.id != s2.id
  and s2.id < b.student_id;

delete from student s1
            using student s2
where s1.gebruikersnaam = s2.gebruikersnaam
  and s1.id > s2.id;
