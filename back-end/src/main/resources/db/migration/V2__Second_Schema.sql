CREATE TABLE IF NOT EXISTS opleidings_instituut
(
    id           bigint primary key,
    naam         varchar(255),
    beschrijving varchar(255),
    url          varchar(255)
);

CREATE TABLE IF NOT EXISTS student
(
    id             bigint primary key,
    gebruikersnaam varchar(255),
    email          varchar(255)
);

CREATE TABLE IF NOT EXISTS studie
(
    id                     bigint primary key,
    duur                   varchar(255),
    kosten                 double precision,
    naam                   varchar(255),
    opleidingsinstituten_id bigint REFERENCES opleidings_instituut (id),
    opleidingsinstituut_id bigint REFERENCES opleidings_instituut (id)
);

CREATE TABLE IF NOT EXISTS beoordeling
(
    id         bigint primary key,
    rating     double precision,
    studie_id  bigint REFERENCES studie (id),
    student_id bigint references student (id)
);
