TRUNCATE TABLE opleidings_instituut CASCADE;
TRUNCATE TABLE studie CASCADE;

INSERT INTO opleidings_instituut (id, naam)
VALUES (1, 'Hogeschool Amsterdam');

INSERT INTO opleidings_instituut (id, naam)
VALUES (2, 'TU Delft');

INSERT INTO opleidings_instituut (id, naam)
VALUES (3, 'Hogeschool Utrecht');

INSERT INTO opleidings_instituut (id, naam)
VALUES (4, 'Universiteit van Amsterdam');

INSERT INTO opleidings_instituut (id, naam)
VALUES (5, 'Universiteit Utrecht');

INSERT INTO opleidings_instituut (id, naam)
VALUES (6, 'Radboud Universiteit');

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (1, 'Biologie', '4 jaar', 1298.67, 1);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (2, 'Biologie', '4 jaar', 1298.67, 2);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (3, 'Biologie', '4 jaar', 1298.67, 3);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (4, 'Biologie', '4 jaar', 1298.67, 4);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (5, 'Biologie', '4 jaar', 1298.67, 5);

INSERT INTO studie (id, naam, duur,  kosten, opleidingsinstituten_id)
VALUES (6, 'Scheikunde', '3 jaar', 1298.67, 1);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (7, 'Technische informatica', '4 jaar', 1298.67, 2);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (8, 'Internationale betrekkingen', '4 jaar', 1298.67, 4);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (9, 'Internationale betrekkingen', '4 jaar', 1298.67, 3);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (10, 'Internationale betrekkingen', '4 jaar', 1298.67, 1);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (11, 'Antropologie', '3 jaar', 1298.67, 5);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (12, 'Antropologie', '3 jaar', 1298.67, 3);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (13, 'Antropologie', '3 jaar', 1298.67, 2);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (14, 'Antropologie', '3 jaar', 1298.67, 4);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (15, 'Antropologie', '3 jaar', 1298.67, 6);

INSERT INTO studie (id, naam, duur,  kosten, opleidingsinstituten_id)
VALUES (16, 'Tandheelkunde', '6 jaar', 1298.67, 6);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (17, 'Tandheelkunde', '6 jaar', 1298.67, 5);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (18, 'Tandheelkunde', '6 jaar', 1298.67, 1);

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituten_id)
VALUES (19, 'Tandheelkunde', '6 jaar', 1298.67, 2);
