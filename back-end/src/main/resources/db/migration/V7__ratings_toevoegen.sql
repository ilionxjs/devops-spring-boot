INSERT INTO opleidings_instituut (id, naam)
VALUES (7, 'Haagsche Hogeschool');

INSERT INTO studie (id, naam, duur, kosten, opleidingsinstituut_id)
VALUES (20, 'Technische informatica', '4 jaar', 1298.67, 7);

INSERT INTO student (id, email, gebruikersnaam)
VALUES (1, 'jason.mcdonald@gmail.com', 'jasonmc');

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (1, 5, 1, 20);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (2, 8, 1, 18);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (3, 4, 1, 3);

INSERT INTO student (id, email, gebruikersnaam)
VALUES (2, 'aliciakeys@gmail.com', 'aliciaforever');

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (4, 6, 2, 5);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (5, 8, 2, 20);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (6, 9, 2, 18);

INSERT INTO student (id, email, gebruikersnaam)
VALUES (3, 'janpieter@hotmail.com', 'jannetje');

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (7, 7, 3, 18);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (8, 3, 3, 4);

INSERT INTO student (id, email, gebruikersnaam)
VALUES (4, 'amberhollart@hotmail.com', 'amhollart');

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (9, 7, 4, 1);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (10, 8, 4, 15);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (11, 6, 4, 19);

INSERT INTO beoordeling (id, rating, student_id, studie_id)
VALUES (12, 8, 4, 20);
