create table studie_opleidingsinstituut (
  studie_id bigint references studie(id),
  opleidingsinstituut_id bigint references opleidings_instituut(id)
);


INSERT INTO studie_opleidingsinstituut
SELECT id,opleidingsinstituut_id FROM studie;

-- SELECT id, opleidingsinstituut_id
-- INTO studie_opleidingsinstituut
-- from studie;

ALTER TABLE beoordeling
    ADD COLUMN opleidings_instituut_id BIGINT;

update beoordeling b
set opleidings_instituut_id = s1.id
from studie s1
where s1.id = s1.opleidingsinstituut_id;

ALTER TABLE studie
DROP opleidingsinstituut_id;


