update studie_opleidingsinstituut soi
set studie_id = s1.id
from studie s1,
     studie s2
where s1.id < s2.id
  and s1.naam = s2.naam
  and soi.studie_id = s2.id;

commit;


update beoordeling b
set studie_id = s3.id
from studie s3,
     studie_opleidingsinstituut soi,
     studie s1
where s3.id = soi.studie_id
  and s3.naam = s1.naam
  and b.studie_id = s1.id
  and b.studie_id not in (select studie_id from studie_opleidingsinstituut);

commit;

delete
from studie
where studie.id not in (select studie_id from studie_opleidingsinstituut);

commit;
