CREATE TABLE question (
  id        bigint PRIMARY KEY,
  question  varchar(255)
);

CREATE TABLE answer (
  id        bigint PRIMARY KEY,
  answer    varchar(1023),
  question_id  bigint REFERENCES question(id)
);

INSERT INTO question (id, question)
    VALUES (1, 'What was your favorite subject?');

INSERT INTO question (id, question)
    VALUES (2, 'What was the easiest subject?');

INSERT INTO question (id, question)
    VALUES (3, 'What was the hardest subject?');

INSERT INTO answer (id, answer, question_id)
    VALUES (1, 'Front End', 1);

INSERT INTO answer (id, answer, question_id)
    VALUES (2, 'Front End', 1);

INSERT INTO answer (id, answer, question_id)
    VALUES (3, 'SQL', 1);

INSERT INTO answer (id, answer, question_id)
    VALUES (4, 'Front End', 2);

INSERT INTO answer (id, answer, question_id)
    VALUES (5, 'Spring boot', 2);

INSERT INTO answer (id, answer, question_id)
    VALUES (6, 'SQL', 2);

INSERT INTO answer (id, answer, question_id)
    VALUES (7, 'Maven', 3);

INSERT INTO answer (id, answer, question_id)
    VALUES (8, 'Back end', 3);

INSERT INTO answer (id, answer, question_id)
    VALUES (9, 'Webserver', 3);


