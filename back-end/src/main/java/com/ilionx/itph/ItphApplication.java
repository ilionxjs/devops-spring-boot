package com.ilionx.itph;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItphApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItphApplication.class, args);
	}
}
