package com.ilionx.itph.config;

import com.ilionx.itph.persistance.opleiding.OpleidingsInstituut;
import com.ilionx.itph.persistance.opleiding.Studie;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(
                OpleidingsInstituut.class,
                Studie.class
        );
    }
}
