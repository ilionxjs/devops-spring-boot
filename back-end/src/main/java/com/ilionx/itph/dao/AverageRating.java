package com.ilionx.itph.dao;

import com.ilionx.itph.persistance.opleiding.OpleidingsInstituut;
import com.ilionx.itph.persistance.opleiding.Studie;

public class AverageRating {
    double rating;
    Studie studie;
    OpleidingsInstituut opleidingsInstituut;

    public AverageRating(double rating, Studie studie, OpleidingsInstituut opleidingsInstituut) {
        this.rating = rating;
        this.studie = studie;
        this.opleidingsInstituut = opleidingsInstituut;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Studie getStudie() {
        return studie;
    }

    public void setStudie(Studie studie) {
        this.studie = studie;
    }

    public OpleidingsInstituut getOpleidingsInstituut() {
        return opleidingsInstituut;
    }

    public void setOpleidingsInstituut(OpleidingsInstituut opleidingsInstituut) {
        this.opleidingsInstituut = opleidingsInstituut;
    }
}
