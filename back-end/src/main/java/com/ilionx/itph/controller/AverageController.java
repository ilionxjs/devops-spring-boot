package com.ilionx.itph.controller;
import com.ilionx.itph.dao.AverageRating;
import com.ilionx.itph.persistance.opleiding.Beoordeling;
import com.ilionx.itph.repository.BeoordelingRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AverageController {
    private final BeoordelingRepository beoordelingRepository;

    public AverageController (BeoordelingRepository beoordelingRepository){
        this.beoordelingRepository = beoordelingRepository;
    }

    @RequestMapping(value="/studies/{studie}", produces="application/json")
    public ResponseEntity average(@PathVariable Long studie) {
        List<AverageRating> averageRatings = beoordelingRepository.getAverageRatingPerInstituteByStudy(studie);

        return ResponseEntity.ok(averageRatings);
    }
}
