package com.ilionx.itph.persistance.account;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.ilionx.itph.persistance.opleiding.Beoordeling;

import javax.persistence.*;
import java.util.List;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "StudentSequence")
    @SequenceGenerator(name = "studentSequence", initialValue = 100, sequenceName = "student_seq")
    private long id;
    private String gebruikersnaam;
    @OneToMany (mappedBy = ("student"))
    @JsonBackReference
    private List<Beoordeling> beoordelingen;
    private String email;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getGebruikersnaam() {
        return gebruikersnaam;
    }


    public void setGebruikersnaam(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }


    public List<Beoordeling> getBeoordelingen() {
        return beoordelingen;
    }

    public void setBeoordelingen(List<Beoordeling> beoordelingen) {
        this.beoordelingen = beoordelingen;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
