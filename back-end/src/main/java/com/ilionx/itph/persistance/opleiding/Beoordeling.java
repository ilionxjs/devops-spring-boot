package com.ilionx.itph.persistance.opleiding;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ilionx.itph.persistance.account.Student;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

@Entity
public class Beoordeling  {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "beoordelingSequence")
    @SequenceGenerator(name = "beoordelingSequence", initialValue = 100, sequenceName = "beoordeling_sequence")
    private long id;
    @ManyToOne
    private Studie studie;
    @ManyToOne
    private Student student;
    private double rating;
    @ManyToOne
    private OpleidingsInstituut opleidingsInstituut;

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public void setStudie(Studie studie) {
        this.studie = studie;
    }

    public Studie getStudie() {
        return studie;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {

        this.student = student;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public OpleidingsInstituut getOpleidingsInstituut() {
        return opleidingsInstituut;
    }

    public void setOpleidingsInstituut(OpleidingsInstituut opleidingsInstituut) {
        this.opleidingsInstituut = opleidingsInstituut;
    }
}

