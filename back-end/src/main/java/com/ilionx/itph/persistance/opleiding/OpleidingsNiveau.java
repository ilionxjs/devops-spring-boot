package com.ilionx.itph.persistance.opleiding;

public enum OpleidingsNiveau {
    MBO, HBO, WO
}
