package com.ilionx.itph.persistance.opleiding;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity
public class Studie {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "studieSequence")
    @SequenceGenerator(name = "studieSequence", initialValue = 100, sequenceName = "studie_seq")
    private Long id;
    private String duur;
    private double kosten;
    private String naam;
    @ManyToMany
    @JoinTable(
            name = "studie_opleidingsinstituut",
            joinColumns = @JoinColumn(name = "studie_id"),
            inverseJoinColumns = @JoinColumn(name = "opleidingsinstituut_id")
    )
    private List<OpleidingsInstituut> opleidingsinstituut;
    @OneToMany (mappedBy = ("studie"))
    @JsonBackReference
    private List<Beoordeling> beoordelingen;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Beoordeling> getBeoordelingen() {
        return beoordelingen;
    }

    public void setBeoordelingen(List<Beoordeling> beoordelingen) {
        this.beoordelingen = beoordelingen;
    }

    public void setDuur(String duur) {
        this.duur = duur;
    }

    public void setKosten(double kosten) {
        this.kosten = kosten;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public void setOpleidingsinstituut(List<OpleidingsInstituut> opleidingsinstituut) {
        this.opleidingsinstituut = opleidingsinstituut;
    }

    public double getKosten() {
        return kosten;
    }

    public String getDuur() {
        return duur;
    }


    public String getNaam() {
        return naam;
    }

    public List<OpleidingsInstituut> getOpleidingsinstituut() {
        return opleidingsinstituut;
    }
}
