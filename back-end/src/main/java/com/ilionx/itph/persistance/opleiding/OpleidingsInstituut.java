package com.ilionx.itph.persistance.opleiding;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
public class OpleidingsInstituut {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "opleidinginstituutSequence")
    @SequenceGenerator(name = "opleidingsinstituutSequence", initialValue = 100, sequenceName = "opleidingsinstituut_seq")
    private long id;
    private String naam;
    private String beschrijving;
    private String url;
    @ManyToMany (mappedBy = "opleidingsinstituut")
    @JsonBackReference
    private List<Studie> studies;
    @OneToMany (mappedBy = "opleidingsInstituut")
    @JsonBackReference
    private List<Beoordeling> beoordelingen;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNaam() { return naam; }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public String getBeschrijving() {
        return beschrijving;
    }

    public void setBeschrijving(String beschrijving) {
        this.beschrijving = beschrijving;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Studie> getStudies() {
        return studies;
    }

    public void setStudies(List<Studie> studies) {
        this.studies = studies;
    }
}
