package com.ilionx.itph.repository;

import com.ilionx.itph.persistance.opleiding.Studie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "studie")
public interface StudieRepository extends JpaRepository<Studie, Long> {
    List<Studie> findStudieByNaamContainingIgnoreCaseAndOpleidingsinstituutId(@Param("naam") String naam, @Param("instituut")Long instituut);
    List<Studie> findStudieByNaamContainingIgnoreCase(@Param("naam") String naam);
}
