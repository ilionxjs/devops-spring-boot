package com.ilionx.itph.repository;

import com.ilionx.itph.persistance.opleiding.OpleidingsInstituut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "opleidings-instituut")
public interface OpleidingsInstituutRepository extends JpaRepository<OpleidingsInstituut, Long> {
    List<OpleidingsInstituut> findByNaamIgnoreCaseContainingAndStudiesId(@Param("naam") String naam, @Param("studie")Long studie);
    List<OpleidingsInstituut> findByNaamIgnoreCaseContaining(@Param("naam") String naam);
}

