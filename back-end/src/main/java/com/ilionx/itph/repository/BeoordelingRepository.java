package com.ilionx.itph.repository;

import com.ilionx.itph.dao.AverageRating;
import com.ilionx.itph.persistance.opleiding.Beoordeling;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path="beoordeling")
public interface BeoordelingRepository extends JpaRepository<Beoordeling, Long> {

    @Query(value = "select new com.ilionx.itph.dao.AverageRating(" +
            "AVG(b.rating), " +
            "b.studie, " +
            "b.opleidingsInstituut" +
            ") from Beoordeling b where b.studie.id = :studieId group by b.studie, b.opleidingsInstituut")
    List<AverageRating> getAverageRatingPerInstituteByStudy(@Param("studieId") Long studieId);

    @Query(value="select b from Beoordeling b where lower(b.studie.naam) || lower(b.student.gebruikersnaam) || lower(b.opleidingsInstituut.naam) like ('%' || lower(:filter) || '%')")
    Page<Beoordeling> searchFilter(@Param("filter") String filter, Pageable pageable);
}
