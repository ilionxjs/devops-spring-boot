package com.ilionx.itph.repository;

import com.ilionx.itph.persistance.Answer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "answer")
public interface AnswerRepository extends JpaRepository<Answer, Long> {

}
