package com.ilionx.itph.repository;

import com.ilionx.itph.persistance.account.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path="student")
public interface StudentRepository extends JpaRepository<Student, Long>{
    Student findStudentByGebruikersnaam(@Param("gebruikersnaam") String gebruikersnaam);
}

