package com.ilionx.itph.repository;

import com.ilionx.itph.persistance.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "question")
public interface QuestionRepository extends JpaRepository<Question, Long> {

}
