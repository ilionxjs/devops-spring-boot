import {NgModule} from "@angular/core";
import {AverageGridComponent} from "./average-grid.component";
import {CommonModule} from "@angular/common";
import {MatTableModule} from "@angular/material";

@NgModule({
  declarations: [AverageGridComponent],
  imports: [
    CommonModule,
    MatTableModule
    ],
  exports: [AverageGridComponent]
})

export class AverageGridModule {}
