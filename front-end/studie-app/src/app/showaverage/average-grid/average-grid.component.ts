import {Component, Input, OnInit} from '@angular/core';
import {AverageRating} from '../../model/averagerating';

@Component({
  selector: 'app-average-grid',
  templateUrl: './average-grid.component.html',
  styleUrls: ['./average-grid.component.css']
})
export class AverageGridComponent implements OnInit {

  @Input() averages: AverageRating[];

  columnsToDisplay: string[] = ['instituut', 'studie', 'rating'];

  constructor() { }

  ngOnInit() {
  }

}
