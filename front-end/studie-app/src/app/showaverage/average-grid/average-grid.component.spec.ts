import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageGridComponent } from './average-grid.component';

describe('AverageGridComponent', () => {
  let component: AverageGridComponent;
  let fixture: ComponentFixture<AverageGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AverageGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AverageGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
