import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {AverageRating} from '../model/averagerating';
import {BeoordelingService} from '../api/beoordeling.service';
import {ActivatedRoute} from '@angular/router';
import {filter, pluck, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-show-average-for-institute',
  templateUrl: './show-average-for-institute.component.html',
  styleUrls: ['./show-average-for-institute.component.css']
})
export class ShowAverageForInstituteComponent implements OnInit {

  averages: Observable<AverageRating[]>;

  constructor(private beoordelingService: BeoordelingService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.averages = this.activatedRoute.params.pipe(
      pluck('id'),
      filter(id => !!id),
      switchMap(id => this.beoordelingService.getAverage(id))
    );
  }

}
