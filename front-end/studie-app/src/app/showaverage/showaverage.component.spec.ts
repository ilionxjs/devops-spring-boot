import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowaverageComponent } from './showaverage.component';

describe('ShowaverageComponent', () => {
  let component: ShowaverageComponent;
  let fixture: ComponentFixture<ShowaverageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowaverageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowaverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
