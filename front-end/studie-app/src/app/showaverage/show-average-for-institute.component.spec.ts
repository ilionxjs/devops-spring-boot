import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAverageForInstituteComponent } from './show-average-for-institute.component';

describe('ShowAverageForInstituteComponent', () => {
  let component: ShowAverageForInstituteComponent;
  let fixture: ComponentFixture<ShowAverageForInstituteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAverageForInstituteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAverageForInstituteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
