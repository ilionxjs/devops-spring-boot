import {Component, OnInit} from '@angular/core';
import {BeoordelingService} from '../api/beoordeling.service';
import {AverageRating} from '../model/averagerating';
import {Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {switchMap} from 'rxjs/operators';
import {Studie} from '../model/studie';

@Component({
  selector: 'app-show-average',
  templateUrl: './showaverage.component.html',
  styleUrls: ['./showaverage.component.css']
})

export class ShowaverageComponent implements OnInit {

  averages: Observable<AverageRating[]>;
  studiekeuze: FormControl = new FormControl();

  constructor(private beoordelingService: BeoordelingService) { }

  ngOnInit() {
    this.averages = this.studiekeuze.valueChanges.pipe(
      switchMap((studie: Studie) => this.beoordelingService.getAverage(studie.id))
    );
  }
}
