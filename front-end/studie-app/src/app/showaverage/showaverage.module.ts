import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ShowaverageComponent} from './showaverage.component';
import {AverageGridModule} from './average-grid/average-grid.module';
import {StudiekeuzeModule} from '../add-review/reviewform/studiekeuze/studiekeuze.module';
import {ReactiveFormsModule} from '@angular/forms';
import { ShowAverageForInstituteComponent } from './show-average-for-institute.component';


@NgModule({
  declarations: [
    ShowaverageComponent,
    ShowAverageForInstituteComponent
  ],
  imports: [
    CommonModule,
    AverageGridModule,
    StudiekeuzeModule,
    ReactiveFormsModule
  ],
  exports: [ShowaverageComponent]
})
export class ShowaverageModule { }
