import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotFoundComponent} from './not-found/not-found.component';
import {HomeComponent} from './home/home.component';
import {AddReviewComponent} from './add-review/add-review.component';
import {ShowreviewComponent} from './showreviews/showreview.component';
import {ShowaverageComponent} from './showaverage/showaverage.component';
import {ShowAverageForInstituteComponent} from './showaverage/show-average-for-institute.component';
import {AuthGuard} from "./auth/auth.guard";

const routes: Routes = [
  {path: 'login',
    loadChildren: () => import('./home/inlog-scherm/inlog-scherm.module').then(mod =>  mod.InlogSchermModule)},
  {path: 'home', component: HomeComponent},
  {path: 'newreview', component: AddReviewComponent, canActivate: [AuthGuard]},
  {path: 'overview', component: ShowreviewComponent, canActivate: [AuthGuard]},
  {path: 'averages', canActivate: [AuthGuard], children: [
      {path: '', component: ShowaverageComponent, pathMatch: 'full'},
      {path: ':id', component: ShowAverageForInstituteComponent}]},
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
