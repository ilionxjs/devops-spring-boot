import { Injectable } from '@angular/core';
import {Student} from './model/student';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  currentUser: Student = undefined;
}
