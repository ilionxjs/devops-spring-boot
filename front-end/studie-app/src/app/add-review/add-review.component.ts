import { Component, OnInit } from '@angular/core';
import {Review} from '../model/review';
import {ReviewService} from '../api/review.service';
import {switchMapTo} from 'rxjs/operators';
import {Studie} from '../model/studie';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {

  constructor(
    private reviewService: ReviewService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onAddBook($event: Review) {
    this.reviewService.storeReview($event).pipe(
      switchMapTo(this.reviewService.getUrl($event.studie))
    ).subscribe((studie: Studie) => {
      this.router.navigate([`/averages/${studie.id}`]);
    });
  }
}
