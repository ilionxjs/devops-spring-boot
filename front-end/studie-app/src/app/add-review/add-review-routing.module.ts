import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddReviewComponent} from './add-review.component';

const routes: Routes = [
  {path: 'add', component: AddReviewComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddReviewRoutingModule { }
