import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OpleidingsInstituutKeuzeComponent} from "./opleidings-instituut-keuze.component";
import {MatAutocompleteModule, MatFormFieldModule, MatInputModule} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [OpleidingsInstituutKeuzeComponent],
  exports: [
    OpleidingsInstituutKeuzeComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatInputModule
  ]
})
export class OpleidingsInstituutKeuzeModule { }
