import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {debounceTime, distinctUntilChanged, filter, switchMap, tap} from 'rxjs/operators';
import {InstituutService} from '../../../api/instituut.service';
import {Observable} from 'rxjs';
import {OpleidingsInstituut} from '../../../model/opleidings-instituut';


@Component({
  selector: 'app-opleidingskeuze',
  templateUrl: './opleidings-instituut-keuze.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => OpleidingsInstituutKeuzeComponent),
    multi: true
  }]
})

export class OpleidingsInstituutKeuzeComponent implements ControlValueAccessor, OnInit {

  suggestions: Observable<OpleidingsInstituut[]>;
  formControl: FormControl = new FormControl();

  @Input() studiesId: number;

  constructor(private instituutService: InstituutService) {
  }

  ngOnInit(): void {
    this.suggestions = this.formControl.valueChanges.pipe(
      filter(formValue => typeof formValue !== 'object'),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(name => this.studiesId ?
        this.instituutService.getOpleidingsInstituutByNameAndStudies(name, this.studiesId):
        this.instituutService.getOpleidingsInstituutByName(name))
      /*
        zorgt ervoor dat de keuze voor een opleidingsinstituut in het formulieer gekozen kan worden.
        als de student toch een ander instituut wil wordt de andere gecancelt
       */
    );
  }

  displayFn(opleidingsinstituut: OpleidingsInstituut): string | undefined {
    return opleidingsinstituut ? opleidingsinstituut.naam : undefined;
    // zorgt ervoor dat de opleidingsinstituut geen object toont maar een naam
  }

  // de vier volgende methodes worden automatisch gegenereerd bij mat-form
  registerOnChange(fn: any): void {
    this.formControl.valueChanges.pipe(
      filter(formValue => typeof formValue === 'object')
    ).subscribe(fn);
  }

  registerOnTouched(fn: any): void {
    this.formControl.valueChanges.subscribe(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    // TODO not implemented
  }

  writeValue(obj: any): void {
    console.log('value set', obj);
  }

}
