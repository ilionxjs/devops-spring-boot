import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewformComponent } from './reviewform.component';
import {StudiekeuzeModule} from './studiekeuze/studiekeuze.module';
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatRadioModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {OpleidingsInstituutKeuzeModule} from './opleidingskeuze/opleidings-instituut-keuze.module';

@NgModule({
  declarations: [ReviewformComponent],
  imports: [
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    StudiekeuzeModule,
    ReactiveFormsModule,
    OpleidingsInstituutKeuzeModule,
    MatButtonModule,
    MatRadioModule
  ],
  exports: [ReviewformComponent]
})
export class ReviewformModule {


}
