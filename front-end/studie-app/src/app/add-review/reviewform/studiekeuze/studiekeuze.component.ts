import {Component, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, mergeMap, switchMap, switchMapTo, tap} from 'rxjs/operators';
import {StudieService} from '../../../api/studie.service';
import {Studie} from '../../../model/studie';
import {InstituutService} from "../../../api/instituut.service";
import {OpleidingsInstituut} from "../../../model/opleidings-instituut";

@Component({
  selector: 'app-studie-keuze',
  templateUrl: './studiekeuze.component.html',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => StudiekeuzeComponent),
    multi: true
  }]
})
export class StudiekeuzeComponent implements ControlValueAccessor,  OnInit {
  suggestions: Observable<Studie[]>;
  formControl: FormControl = new FormControl();

  @Input() instituutId: number;
  @Input() canAdd = false;

  studieToegevoegd = false;

  constructor(
    private studieService: StudieService,
    private instituutService: InstituutService
  ) {
  }

  ngOnInit(): void {
    this.suggestions = this.formControl.valueChanges.pipe(
      filter(formValue => typeof formValue !== 'object'),
      debounceTime(400),
      distinctUntilChanged(),
      tap(() => this.studieToegevoegd = false),
      switchMap(name => this.instituutId ?
        this.studieService.getStudieByNameAndInstituut(name, this.instituutId) :
        this.studieService.getStudieByName(name))
    );
  }
  registerOnChange(fn: any): void {
    this.formControl.valueChanges.pipe(
      filter(formValue => typeof formValue === 'object'),
    ).subscribe(fn);
  }
  displayFn(studie: Studie): string | undefined {
    return studie ? studie.naam : undefined;
  }

  registerOnTouched(fn: any): void {
    this.formControl.valueChanges.subscribe(fn);
  }

  setDisabledState(isDisabled: boolean): void {
    // TODO not implemented
  }

  writeValue(obj: any): void {
    console.log('value set', obj);
  }

  addNewStudie() {
    this.instituutService.getOpleidingsInstituut(this.instituutId).pipe(
      mergeMap((instituut: OpleidingsInstituut) => this.studieService.postNewStudie(this.formControl.value, instituut._links.self.href))
    ).subscribe
    (newStudie => {
      this.formControl.setValue(newStudie);
      this.studieToegevoegd = true;
    });
  }
}
