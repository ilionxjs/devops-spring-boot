import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StudiekeuzeComponent} from "./studiekeuze.component";
import {MatAutocompleteModule, MatFormFieldModule, MatInputModule} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [StudiekeuzeComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatAutocompleteModule
  ],
  exports: [StudiekeuzeComponent
   ]
})
export class StudiekeuzeModule { }
