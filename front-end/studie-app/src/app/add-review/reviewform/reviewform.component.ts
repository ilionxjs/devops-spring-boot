import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Review} from '../../model/review';
import {Studie} from '../../model/studie';
import {Observable} from 'rxjs';
import {pluck, tap} from 'rxjs/operators';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OpleidingsInstituut} from '../../model/opleidings-instituut';
import {UserService} from '../../user.service';


@Component({
  selector: 'app-reviewform',
  templateUrl: './reviewform.component.html',
  styleUrls: ['./reviewform.component.css']
})
export class ReviewformComponent implements OnInit {

  reviewForm: FormGroup;
  instituut: Observable<number>;
  studiekeuze: Observable<number>;
  @Output() reviewValue = new EventEmitter<Review>();

  constructor(
    private fb: FormBuilder,
    private userService: UserService
  ) {
  }


  ngOnInit() {
    this.reviewForm = this.fb.group({
      rating: 10,
      studiekeuze: null,
      instituut: null
    });


    this.instituut = this.reviewForm.get('instituut').valueChanges.pipe(
      pluck('id')
    );

    this.studiekeuze = this.reviewForm.get('studiekeuze').valueChanges.pipe(
      pluck('id')
    );

  }

  sendReview() {
    if (this.reviewForm.valid) {
      const review: Review = {
        rating: this.reviewForm.value.rating,
        student: this.userService.currentUser,
        studie: (this.reviewForm.value.studiekeuze as Studie)._links.self.href,
        opleidingsInstituut: (this.reviewForm.value.instituut as OpleidingsInstituut)._links.self.href,
      };
      this.reviewValue.emit(review);
    } else {
      console.log('form not valid');
      /*geeft aan welke gegevens precies gestuurd moeten worden
       tevens geeft een link aan studie zodat deze gevonden kan worden
       */
    }
  }

}
