import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddReviewRoutingModule } from './add-review-routing.module';
import {AddReviewComponent} from './add-review.component';
import {ReviewformModule} from './reviewform/reviewform.module';
import {RouterModule} from "@angular/router";


@NgModule({
  declarations: [AddReviewComponent],
  imports: [
    CommonModule,
    AddReviewRoutingModule,
    ReviewformModule,
    RouterModule
  ],
  exports: [AddReviewComponent]
})
export class AddReviewModule { }
