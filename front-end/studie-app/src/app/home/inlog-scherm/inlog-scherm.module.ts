import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InlogSchermComponent } from './inlog-scherm.component';
import {
  MatFormFieldControl,
  MatFormFieldModule, MatInputModule,
  MatOptionModule,
  MatRadioModule,
  MatSelectModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [InlogSchermComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatInputModule,

  ],
  exports: [
    InlogSchermComponent
  ]
})
export class InlogSchermModule {

}
