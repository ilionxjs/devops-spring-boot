import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {InlogService} from '../../api/inlog.service';
import {UserService} from '../../user.service';

@Component({
  selector: 'app-inlog-scherm',
  templateUrl: './inlog-scherm.component.html',
  styleUrls: ['./inlog-scherm.component.css']
})
export class InlogSchermComponent implements OnInit {
  inlog: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,
              private gebruikersnaamService: InlogService,
              private userService: UserService) {
  }

  ngOnInit() {
    this.inlog = this.fb.group({
      keuze: 'instituut',
      gebruikersnaam: [''],
    });
  }

  addlogin() {
    this.gebruikersnaamService.getStudentByGebruikersnaam(this.inlog.value.gebruikersnaam).subscribe(
      user => {
        this.userService.currentUser = user;
        this.inlog.reset();
        this.router.navigate(['/newreview']);
      },
      error => {
        this.gebruikersnaam.setErrors({validUser: false});
      });
  }

  get gebruikersnaam() {
    return this.inlog.get('gebruikersnaam');
  }
}
