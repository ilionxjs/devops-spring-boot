import {NgModule} from "@angular/core";
import {ShowreviewComponent} from "./showreview.component";
import {CommonModule} from "@angular/common";
import {ReviewGridModule} from "./review-grid/review-grid.module";
import {MatInputModule, MatPaginatorModule} from "@angular/material";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FilterModule} from "./filter/filter.module";

@NgModule({
  declarations: [ShowreviewComponent],
  imports: [CommonModule, ReviewGridModule,
    MatPaginatorModule, MatFormFieldModule, MatInputModule, FilterModule],
  exports: [ShowreviewComponent]
})
export class ShowreviewModule {}
