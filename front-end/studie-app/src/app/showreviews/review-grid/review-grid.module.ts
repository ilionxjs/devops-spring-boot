import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReviewGridComponent } from './review-grid.component';
import {MatPaginatorModule, MatTableModule} from "@angular/material";



@NgModule({
  declarations: [ReviewGridComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatPaginatorModule

  ],
  exports: [ReviewGridComponent]
})
export class ReviewGridModule { }
