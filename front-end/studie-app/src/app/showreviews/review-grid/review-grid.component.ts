import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Beoordeling} from "../../model/beoordeling";
import {PageEvent} from "@angular/material";

@Component({
  selector: 'app-review-grid',
  templateUrl: './review-grid.component.html',
  styleUrls: ['./review-grid.component.css']
})
export class ReviewGridComponent {

  // hier halen we de beoordelingen mee op
  @Input() beoordelingen: Beoordeling[];
  // hier halen we het aantal pagina's op
  @Input()
  aantal: number;
  @Input()
  pageSize: number;

  columnsToDisplay: string[] = ['naam', 'instituut', 'studie', 'rating'];
  // hier zorgen we ervoor dat de pagina's worden opgeroepen
  @Output() pageChange = new EventEmitter<PageEvent>();

  onPageChanged($event: PageEvent) {
    console.log($event);
    return this.pageChange.emit($event);
  }
}




