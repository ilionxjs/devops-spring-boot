import {Injectable} from '@angular/core';
import {BeoordelingService} from '../api/beoordeling.service';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map, pluck, share, switchMap} from 'rxjs/operators';
import {Beoordeling} from '../model/beoordeling';
import {Student} from '../model/student';
import {Studie} from '../model/studie';
import {OpleidingsInstituut} from '../model/opleidings-instituut';
import {Beoordelingen} from "../model/beoordelingen";


export interface DisplayBeoordeling {
  naam: string;
  studie: string;
  instituut: string;
  rating: number;
}


export interface DisplayBeoordelingPage {
  beoordelingen: Observable<DisplayBeoordeling[]>;
  aantal: Observable<number>;
}



@Injectable({
  providedIn: 'root'
})
export class ShowReviewService {

  constructor(private beoordelingService: BeoordelingService) {
  }

  /**
   * Fetch any page of ratings
   * @param page
   */
  getNextFilteredPage(page: number, filter?: string): DisplayBeoordelingPage {
    if(filter) {
      return this.createDisplayBeoordelingPage(this.beoordelingService.getNextFilteredPage(page, filter));
    } else {
      return this.createDisplayBeoordelingPage(this.beoordelingService.getNextPage(page));
    }
  }


  /**
   * Create a display object with page info
   */
  private createDisplayBeoordelingPage(beoordelingen$: Observable<Beoordelingen>): DisplayBeoordelingPage {
    const cachedBeoordelingen = beoordelingen$.pipe(
      share()
    );

    const beoordelingen: Observable<DisplayBeoordeling[]> = cachedBeoordelingen.pipe(
      pluck<Beoordelingen, Beoordeling[]>('_embedded', 'beoordelings'),
      switchMap(beoordelings => forkJoin(beoordelings.map(beoordeling => this.createDisplayBeoordeling(beoordeling))))
    );

    const aantal: Observable<number> = cachedBeoordelingen.pipe(
      pluck('page', 'totalElements')
    );

    return {
      beoordelingen,
      aantal
    };
  }

  /**
   * Create a display object containing all information to be shown in the grid
   */
  private createDisplayBeoordeling(beoordeling: Beoordeling): Observable<DisplayBeoordeling> {
    const student$ = this.beoordelingService.getLinkedData<Student>(beoordeling._links.student).pipe(
      catchError(error => of({gebruikersnaam: 'onbekend'} as Student))
    );

    const studie$ = this.beoordelingService.getLinkedData<Studie>(beoordeling._links.studie).pipe(
      catchError(error => of({naam: 'onbekend'} as Studie)),
      share()
    );

    const instituut$ = this.beoordelingService.getLinkedData<OpleidingsInstituut>(beoordeling._links.opleidingsInstituut).pipe(
      catchError(error => of({naam: 'onbekend'} as OpleidingsInstituut)),
    );

    return forkJoin([
      student$, studie$, instituut$
    ]).pipe(
      map(([student, studie, instituut]) => this.getDisplayObject(beoordeling, student, studie, instituut))
    );
  }

  private getDisplayObject(beoordeling: Beoordeling, student: Student, studie: Studie, instituut: OpleidingsInstituut): DisplayBeoordeling {
    return {
      rating: beoordeling.rating,
      studie: studie.naam,
      instituut: instituut.naam,
      naam: student.gebruikersnaam
    };
  }


}
