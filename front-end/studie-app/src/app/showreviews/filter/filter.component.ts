import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  @Output() filter: EventEmitter<string> = new EventEmitter();
  filterCtrl: FormControl = new FormControl();

  ngOnInit(): void {
    this.filterCtrl.valueChanges.subscribe(this.filter);
  }
}
