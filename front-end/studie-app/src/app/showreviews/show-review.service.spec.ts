import { TestBed } from '@angular/core/testing';

import { ShowReviewService } from './show-review.service';

describe('ShowReviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShowReviewService = TestBed.get(ShowReviewService);
    expect(service).toBeTruthy();
  });
});
