import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Observable} from 'rxjs';
import {DisplayBeoordeling, DisplayBeoordelingPage, ShowReviewService} from './show-review.service';
import {PageEvent} from "@angular/material";

@Component({
  selector: 'app-show-review',
  templateUrl: './showreview.component.html'
})
export class ShowreviewComponent implements OnInit, OnDestroy {
  beoordelingen: Observable<DisplayBeoordeling[]>;
  aantal: Observable<number>;


  private $page: BehaviorSubject<number> = new BehaviorSubject(0);
  private $filter: BehaviorSubject<string> = new BehaviorSubject(undefined);

  constructor(private showReviewService: ShowReviewService) {

  }

  ngOnInit(): void {
    combineLatest([this.$page, this.$filter]).subscribe(
      ([page, filter]) => this.fetchPage(page, filter)
    );
  }

  ngOnDestroy(): void {
    this.$filter.complete();
    this.$page.complete();
  }

  receiveChangedPage($event: PageEvent) {
    this.$page.next($event.pageIndex);
  }

  private fetchPage(page: number, filter?: string) {
    const beoordelingenPage: DisplayBeoordelingPage = this.showReviewService.getNextFilteredPage(page, filter);
    this.beoordelingen = beoordelingenPage.beoordelingen;
    this.aantal = beoordelingenPage.aantal;
  }

  onFilter($event: string) {
    this.$filter.next($event);
  }
}
