import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {HttpClientModule} from "@angular/common/http";
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import {AddReviewModule} from './add-review/add-review.module';
import {ShowreviewModule} from "./showreviews/showreview.module";
import {ShowaverageModule} from "./showaverage/showaverage.module";
import {InlogSchermModule} from "./home/inlog-scherm/inlog-scherm.module";



@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AddReviewModule,
    HttpClientModule,
    ShowreviewModule,
    ShowaverageModule,
    InlogSchermModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
