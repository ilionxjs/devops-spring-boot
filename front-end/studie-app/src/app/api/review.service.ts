import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Review} from '../model/review';
import {Observable,} from 'rxjs';
import {Student} from '../model/student';
import {mergeMap} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class ReviewService {
  constructor(private http: HttpClient) {

  }

  storeReview(review: Review): Observable<Review> {
    return this.http.post<null>(
      './api/beoordeling',
      {
        ...review,
        student: review.student._links.self.href
        // haalt student alleen op en slaat hem op vervogens wordt deze link meegestuurd.
        // als de links is opgehaald wordt deze meegestuurd met de review
      }
    );
  }

  getUrl(url: string) {
    return this.http.get(url);
  }
}

