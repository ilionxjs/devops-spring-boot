import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Beoordelingen} from '../model/beoordelingen';
import {Link} from '../model/link';
import {Beoordeling} from "../model/beoordeling";
import {map, pluck} from "rxjs/operators";
import {AverageRating} from "../model/averagerating";

@Injectable({providedIn: 'root'}
)
export class BeoordelingService {

  constructor(private http: HttpClient) {
  }

  getNextPage(page: number): Observable<Beoordelingen> {
    return this.http.get<Beoordelingen>(`api/beoordeling?page=${page}`);
  }

  getAverage(studie: number): Observable<AverageRating[]> {
    return this.http.get<AverageRating[]>(`api/studies/${studie}`);
  }


  getNextFilteredPage(page: number, filter: string): Observable<Beoordelingen> {
    return this.http.get<Beoordelingen>(`api/beoordeling/search/searchFilter?page=${page}&filter=${filter}`);
  }

  getLinkedData<T>(link: Link): Observable<T> {
    return this.http.get<T>(link.href);
  }
}
