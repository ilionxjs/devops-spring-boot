import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, pipe} from "rxjs";
import {map} from "rxjs/operators";
import {Studie} from "../model/studie";
import {Studies} from "../model/studies";

@Injectable({
  providedIn: 'root'
})
export class StudieService {

  constructor(private http: HttpClient) {
  }

  getStudieByNameAndInstituut(naam: string, id: number): Observable<Studie[]> {
    return this.http.get<Studies>(`api/studie/search/findStudieByNaamContainingIgnoreCaseAndOpleidingsinstituutId?naam=${naam}&instituut=${id}`)
      .pipe(
        map(studies => studies._embedded.studies)
      );
  }

  getStudieByName(naam: string): Observable<Studie[]> {
    return this.http.get<Studies>(`api/studie/search/findStudieByNaamContainingIgnoreCase?naam=${naam}`)
      .pipe(
        map(studies => studies._embedded.studies)
      );
  }

  postNewStudie(naam: string,  url: string): Observable<Studie> {
    return this.http.post<Studie>(`api/studie`, {naam, opleidingsinstituut: [url]} );
  }
}
