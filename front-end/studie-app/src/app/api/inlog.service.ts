import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Student} from "../model/student";

@Injectable({
  providedIn: 'root'
})
export class InlogService {

  constructor(private http: HttpClient) {

  }

  getStudentByGebruikersnaam(gebruikersnaam: string): Observable<Student> {
    return this.http.get<Student>(`api/student/search/findStudentByGebruikersnaam?gebruikersnaam=${gebruikersnaam}`);
  }
}
