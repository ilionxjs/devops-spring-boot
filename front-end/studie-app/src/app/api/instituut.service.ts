import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {OpleidingsInstituut} from '../model/opleidings-instituut';
import {OpleidingsInstituten} from '../model/opleidings-instituten';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InstituutService {

  constructor(private http: HttpClient) {

  }

  getOpleidingsInstituutByName(naam: string): Observable<OpleidingsInstituut[]> {
    return this.http.get<OpleidingsInstituten>(`api/opleidings-instituut/search/findByNaamIgnoreCaseContaining?naam=${naam}`)
      .pipe(
        map(instituten => instituten._embedded.opleidingsInstituuts)
      )
  }

  getOpleidingsInstituutByNameAndStudies(naam: string, id: number): Observable<OpleidingsInstituut[]> {
    return this.http.get<OpleidingsInstituten>(`api/opleidings-instituut/search/findByNaamIgnoreCaseContainingAndStudiesId?naam=${naam}&studie=${id}`)
      .pipe(
        map(instituten => instituten._embedded.opleidingsInstituuts)
      );
  }

  getOpleidingsInstituut(instituutId: number): Observable<OpleidingsInstituut> {
    return this.http.get<OpleidingsInstituut>(`api/opleidings-instituut/${instituutId}`);
  }
}
