import { TestBed } from '@angular/core/testing';

import { InstituutService } from './instituut.service';

describe('InstituutService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InstituutService = TestBed.get(InstituutService);
    expect(service).toBeTruthy();
  });
});
