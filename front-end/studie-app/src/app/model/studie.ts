import {Link} from './link';

export interface Studie {
  id: number;
  duur: string;
  kosten: number;
  naam: string;
  _links: {
    self: Link,
    opleidingsinstituut: Link
  };
}
