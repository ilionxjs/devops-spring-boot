export interface Page {
  page: {
    size: number
    totalElements: number
    totalPages: number
    number: number
  };

}
