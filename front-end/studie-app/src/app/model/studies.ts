import {Studie} from "./studie";

export interface Studies{

  _embedded: {
    studies: Studie[]
  };

  _links: {
    self: {
      href: string
    }

}}
