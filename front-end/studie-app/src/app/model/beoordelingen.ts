import {Link} from "./link";
import {Page} from "./page";

export interface Beoordelingen {
  page: Page;
  _embedded: {
    beoordelings: Beoordelingen
    opleidingsInstituut: Link
  };
  _links: {
    self: Link
    href: string

    };

}

