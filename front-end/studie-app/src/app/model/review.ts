import {Student} from "./student";
import {Link} from "./link";

export interface Review {
  rating: number;
  student: Student;
  studie: string;
  opleidingsInstituut: string;
  _links?: {
    self: Link,
  };
}
