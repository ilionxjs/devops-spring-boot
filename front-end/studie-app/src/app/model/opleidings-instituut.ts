import {Link} from "./link";

export interface OpleidingsInstituut {
  id: number;
  naam: string;
  beschrijving: string;
  url: string;
  _links: {
    self: Link
  };
}
