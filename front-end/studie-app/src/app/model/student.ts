import {Link} from './link';

export interface Student {
  gebruikersnaam: string;

  _links?: {
    self: Link
  };
}
