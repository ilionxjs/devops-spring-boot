import {OpleidingsInstituut} from './opleidings-instituut';
import {Link} from './link';

export interface OpleidingsInstituten {
  _embedded: {
    opleidingsInstituuts: OpleidingsInstituut[]
  };
  _links: {
  self: Link

  href: string
};
}

