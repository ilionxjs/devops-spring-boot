import {Studie} from "./studie";
import {OpleidingsInstituut} from "./opleidings-instituut";

export interface AverageRating {
  studie: Studie;
  opleidingsinstituut: OpleidingsInstituut;
  average: number;
}
