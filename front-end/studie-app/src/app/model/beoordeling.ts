import {Link} from "./link";

export interface Beoordeling {
  id: number;
  rating: number;
  _links: {
    opleidingsInstituut: Link;
    student: Link,
    studie: Link

};
}

