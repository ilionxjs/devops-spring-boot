import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'studie-app';

  constructor(
    private router: Router,
    public userService: UserService
  ) {}

  goHome() {
    this.router.navigate(['/']);
  }

  logOut() {
    this.userService.currentUser = undefined;
  }
}
